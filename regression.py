#!/usr/bin/env python3
import pandas
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import math
from math import sqrt

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

from pandas.plotting import autocorrelation_plot
from pandas.plotting import lag_plot
import scipy.stats
from scipy.stats import linregress

from src import modelclass
from src import regressiondataset
from src import experiments

import os

csfont = {'fontname':'Arial'}
hfont = {'fontname':'Arial'}

props = dict(boxstyle='round', facecolor='darkorange', alpha=0.75)

# statistics
def bland_altman_plot(data1, data2, *args, **kwargs):
    data1     = np.asarray(data1)
    data2     = np.asarray(data2)
    mean      = np.mean([data1, data2], axis=0)
    diff      = data1 - data2                   # Difference between data1 and data2
    mn        = np.mean(diff)                   # Mean of the difference
    sd        = np.std(diff, axis=0)            # Standard deviation of the difference
    print('mean:',mn)
    print('std:',sd)
    print('LA95:',sd*1.96)

    plt.plot(mean, diff, 'ok', markerfacecolor='k', markersize=4, alpha = 0.25)
    plt.axhline(mn,             color='k', linestyle='--')
    plt.axhline(mn + 1.96 * sd, color='k', linestyle='--')
    plt.axhline(mn - 1.96 * sd, color='k', linestyle='--')
    # plt.axhline(mn + 2.58 * sd, color='k', linestyle='-')
    # plt.axhline(mn - 2.58 * sd, color='k', linestyle='-')

    return mn, sd

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson_def(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff

    return diffprod / math.sqrt(xdiff2 * ydiff2)

my_batch_size = 20
my_timesteps = 40
my_data_dim = 4

# in trial 1 we train the NN with the incremental to exhaustion test, with the protocol II and with the Wingate test.
# We test the NN on the protocol I test
my_trial_list = (1, 2)

# in trial 2 we train the NN with the incremental to exhaustion test, with the protocol I and II.
# We test the NN on the Wingate test

my_json_list = []
for files in os.listdir('data_int'):
    if files.endswith(".json"):
        my_json_list.append(files)

my_json_list.remove('bonus_sbj.json')
# pre training: train the NN with all the participants except the one that you want to test (only 2 epochs)
# pre training dataset
rd = regressiondataset.RegressionDataset(my_timesteps, my_data_dim)

for my_json_file in my_json_list:

    # pre training
    pre_training_list = list()
    for files in os.listdir('data_int'):
        if files.endswith(".json"):
            pre_training_list.append(files)

    pre_training_list.remove('bonus_sbj.json')
    pre_training_list.remove(my_json_file)

    for my_remaining_files in pre_training_list:

        print(my_remaining_files)
        # my_trial == 1 and you use all the sessions for pre-training
        my_trial = 1
        # read data base
        ex = experiments.Experiments('data_int/' + my_remaining_files, zero_one=True, trial=my_trial)
        # create dataset
        rd.append(ex)

    # pre-training -> create a NEW model for every subject
    md = modelclass.Model(my_batch_size, my_timesteps, my_data_dim)
    dataset_x, dataset_y = md.pre_training(rd)

    for my_trial in my_trial_list:
        # read database
        ex = experiments.Experiments('data_int/' + my_json_file, zero_one=True, trial=my_trial)
        # create dataset
        rd = regressiondataset.RegressionDataset(my_timesteps, my_data_dim)
        rd.append(ex)
        training_index = ex._get_threshold_value(my_trial, my_timesteps)
        dataset_x, dataset_y = md.fit(rd, training_index)
        x0, a, b = md.predict(rd, training_index)

        # compute residuals
        residuals = a - b

        # statistics
        MAE = mean_absolute_error(a, b)
        MSE = mean_squared_error(a, b)
        RMSE = sqrt(MSE)

        # plot figure
        #
        textstr = '\n'.join((
            r'MAE=%.2f' % (MAE * 100,),
            r'MSE=%.2f' % (MSE * 100,),
            r'RMSE=%.2f' % (RMSE * 100,)))
        #
        plt.figure()
        gs = gridspec.GridSpec(2, 1, height_ratios=[3,1])
        plt.subplot(gs[0])
        plt.title('Predictions of AI regressor ' + my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        plt.ylabel('$VO_2$ [%$VO_{2MAX}$]', **hfont)
        plt.plot(a * 100, 'ok', markerfacecolor='k', markersize=4, alpha = 0.25)
        plt.plot(a * 100, '-', color='dodgerblue')
        plt.plot(b * 100, 'r', linewidth=2.0)
        plt.grid(True)
        plt.ylim(-5, 120)
        plt.text(500, 35, textstr, fontsize=14,
                verticalalignment='top', bbox=props)
        plt.subplot(gs[1])
        plt.plot((a-b) * 100, 'k', linewidth=0.5)
        plt.grid(True)
        plt.ylabel('Residuals', **hfont)
        plt.xlabel('Time [s]', **hfont)
        plt.ylim(-50, 50)
        plt.savefig('out_fig/' + 'residuals_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()

        #
        fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1)
        plt.suptitle('Training set ' + my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        ax1.plot(dataset_x[:, 0, 0], '-', color='black')
        ax1.set_ylabel('P', **hfont)
        ax1.get_xaxis().set_visible(False)
        ax2.plot(dataset_x[:, 0, 1], '-', color='goldenrod')
        ax2.set_ylabel('Rf', **hfont)
        ax2.get_xaxis().set_visible(False)
        ax3.plot(dataset_x[:, 0, 2], '-', color='orangered')
        ax3.set_ylabel('HR', **hfont)
        ax3.get_xaxis().set_visible(False)
        ax4.plot(dataset_x[:, 0, 3], '-', color='forestgreen')
        ax4.set_ylabel('cadence', **hfont)
        ax4.get_xaxis().set_visible(False)
        ax5.plot(dataset_y, '-', color='dodgerblue')
        ax5.set_xlabel('time [s]', **hfont)
        ax5.set_ylabel('VO2', **hfont)
        plt.savefig('out_fig/' + 'training_set_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()

        #
        fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1)
        plt.suptitle('Test set ' + my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        ax1.plot(x0[:, 0, 0], '-', color='black')
        ax1.set_ylabel('P', **hfont)
        ax1.get_xaxis().set_visible(False)
        ax2.plot(x0[:, 0, 1], '-', color='goldenrod')
        ax2.set_ylabel('Rf', **hfont)
        ax2.get_xaxis().set_visible(False)
        ax3.plot(x0[:, 0, 2], '-', color='orangered')
        ax3.set_ylabel('HR', **hfont)
        ax3.get_xaxis().set_visible(False)
        ax4.plot(x0[:, 0, 3], '-', color='forestgreen')
        ax4.set_ylabel('cadence')
        ax4.get_xaxis().set_visible(True)
        ax4.set_xlabel('time [s]', **hfont)
        ax5.set_ylabel('VO2??', **hfont)
        ax5.get_xaxis().set_visible(False)
        ax5.get_yaxis().set_visible(True)
        ax5.spines["top"].set_visible(False)
        ax5.spines["right"].set_visible(False)
        ax5.patch.set_alpha(0)
        plt.savefig('out_fig/' + 'test_set_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()
        #
        #
        # autocorrelation plot
        autocorrelation_plot(residuals, linestyle='-', color='dodgerblue')
        plt.title('Autocorrelation Plot ' + my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        plt.savefig('out_fig/' + 'autocorrelation_plot_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()
        #
        # # lag plot
        # # plt.figure()
        # # lag_plot(pandas.DataFrame(residuals))
        #
        # bland altman plot

        plt.figure()
        mn, sd = bland_altman_plot(a * 100, b * 100)

        textstr = '\n'.join((
            r'mean=%.2f' % (mn,),
            r'sd=%.2f' % (sd,)))

        plt.title('Bland-Altman Plot '+ my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        plt.xlabel('Mean [%VO2MAX]', **hfont)
        plt.ylabel('Difference [%VO2MAX]', **hfont)
        plt.grid(True)
        plt.text(20, 5, textstr, fontsize=14,
                verticalalignment='top', bbox=props)
        plt.savefig('out_fig/' + 'bland_altman_plot_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()

        # best line fitting
        r_pearson = pearson_def(a, b)

        textstr = ''.join(('r=%.2f' % (r_pearson,)))

        x = a.flatten()* 100
        y = b.flatten() * 100
        fit = np.polyfit(x, y, deg=1)
        plt.figure()
        plt.plot(x, y, 'ok', markerfacecolor='k', markersize=4, alpha = 0.25)
        plt.plot(x, fit[0] * x + fit[1], color='red')
        plt.title('Correlation plot ' + my_json_file[:-5] + ' trial ' + str(my_trial), **csfont)
        plt.xlabel('Measured [%VO2MAX]', **hfont)
        plt.ylabel('Predicted [%VO2MAX]', **hfont)
        plt.text(5, 40, textstr, fontsize=14,
                verticalalignment='top', bbox=props)
        plt.xlim((0, 110))
        plt.ylim((0, 110))
        plt.grid(True)
        plt.savefig('out_fig/' + 'correlation_plot_' + my_json_file[:-5] + '_trial_' + str(my_trial) + '.png')
        plt.close()

        # print results to screen
        print('Analysis of ' + my_json_file[:-5] + ' in trial' + str(my_trial) + ' completed')
        print('The Pearson r is: ', r_pearson[0])
        print('The variance explained is: ', r_pearson[0] ** 2)
        print('MAE: ', MAE)
        print('RMSE: ', RMSE)
        print('EOF')

        # export data in txt file
        data_file = open('statistics/data_'  + my_json_file[:-5] + '_trial_' + str(my_trial) + '.txt', 'w+')

        data_file.write('t\tP\tRf\tHR\tCad\tTarget_O2\tNN_O2\n')

        for i in np.arange(0,len(a)):
            data_file.write(str(i) + '\t' + str(round(x0[i,0,0],3)) + '\t' + str(round(x0[i,0,1],3)) + '\t' + str(round(x0[i,0,2],3))  + '\t' + str(round(x0[i,0,3],3))  + '\t' + str(a[i].round(3)[0]) + '\t' + str(b[i].round(3)[0]) + '\n')

        data_file.close()
        #
    dummy = 1

dummy = 1



