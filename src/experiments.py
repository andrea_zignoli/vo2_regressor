import pandas
import matplotlib.pyplot as plt
import numpy as np

class Experiments:

    def __init__(self, json_file, zero_one=True, trial=1):
        r"""
        Initialize a new set of Experiments. Requires a json
        file as input that contains the position of
        the different cvs files. For a list of properties
        that must exist inside the csv file please check
        the `_checkConfig` documentation.

        If `zero_one` is True, the normalization is in 0:1
        instead of -1:1.
        """
        from json import load
        from os.path import isfile, abspath, dirname
        if not isfile(json_file):
            raise RuntimeError(
              "The file {} does not exist".format(json_file))
        self.basepath = dirname(abspath(json_file))
        # end if
        with open(json_file, "r") as json_file_p:
            self.config = load(json_file_p)
            self._checkConfig(trial)
        # end with
        self._zero_one = zero_one
        if trial==1:
            self._readCSV_trial_1()
        if trial==2:
            self._readCSV_trial_2()

    def _readCSV_trial_1(self):
        from os.path import join
        self.raw = [
            pandas.read_csv(
                join(self.basepath, fn), header=1,
                names=self.config['input'], engine='python', dtype=float)
            for fn in self.config['csv_trial_1']]

        self.normal = [self._normalizeExperiment(d) for d in self.raw]
    # end def

    def _get_threshold_value(self, trial, my_timesteps):
        from os.path import join
        if trial == 1:
            list_1 = [
                pandas.read_csv(
                    join(self.basepath, fn), header=1,
                    names=self.config['input'], engine='python', dtype=float)
                for fn in self.config['csv_trial_1'][:-1]]

        if trial == 2:
            list_1 = [
                pandas.read_csv(
                    join(self.basepath, fn), header=1,
                    names=self.config['input'], engine='python', dtype=float)
                for fn in self.config['csv_trial_2'][:-1]]

        count1 = 0
        for listElem in list_1:
            count1 += len(listElem)

        count1 = count1 - (my_timesteps + 1)

        return count1
    # end def

    def _readCSV_trial_2(self):
        from os.path import join
        self.raw = [
            pandas.read_csv(
                join(self.basepath, fn), header=1,
                names=self.config['input'], engine='python', dtype=float)
            for fn in self.config['csv_trial_2']]

        self.normal = [self._normalizeExperiment(d) for d in self.raw]
    # end def

    def _normalizeExperiment(self, d):
        norm = d.copy()
        for i in range(1, len(self.config['input'])):
            label = self.config['input'][i]
            if (label + '.max') in self.config:
                coeff = self.config[label + '.max']
            else:
                coeff = np.max(norm[label].values)
            # end if
            if not self._zero_one:
                # Normalization between -1:1
                norm[label] = (norm[label] / coeff) * 2 - 1
            else:
                # Normalization 0:1
                norm[label] = (norm[label] / coeff)
        # end for
        return norm
    # end def

    def save(self, filename=None):
        import pickle
        import os.path

        if filename is None:
            filename = self.config['name'] + '.pickle'
        with open(filename, "wb") as fp:
            pickle.dump(self, fp, pickle.HIGHEST_PROTOCOL)

    def _checkConfig(self, trial):
        if trial == 1:
            checks = {
                'csv_trial_1': [list, 'Not finding the list'],
                'input': [list, 'Missing headers columns csv file']
            }
        if trial == 2:
            checks = {
                'csv_trial_2': [list, 'Not finding the list'],
                'input': [list, 'Missing headers columns csv file']
            }
        for k in checks:
            assert k in self.config, checks[k][1]
            assert (type(self.config[k]) is checks[k][0]), \
                "{} must be of type {}".format(k, checks[k][0])

        # for _, k in enumerate(self.config['input'][1:-1]):
        #    if not (k + '.max' in self.config):
        #        print("Warning: {} normalization not found, maximum will be used".format(k))

    def __len__(self):
        return len(self.raw)
    # end def

    def __getitem__(self, key):
        if key >= 0:
            return self.normal[key]
        else:
            return self.raw[-key - 1]