from tensorflow import keras

import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import LSTM
from tensorflow.python.keras.layers import Dropout

# from keras.models import Sequential
# from keras.layers import Dense, LSTM

class Model:
    def __init__(self, batch_size, timesteps, data_dim):
        self.batch_size = batch_size
        self.model = Sequential()

        self.model.add(LSTM(32, return_sequences=True, stateful=True, batch_input_shape=(batch_size, timesteps, data_dim)))
        self.model.add(Dropout(0.5))
        self.model.add(LSTM(32, return_sequences=True, stateful=True))
        self.model.add(LSTM(32, stateful=True))
        self.model.add(Dense(10, activation='sigmoid'))
        self.model.add(Dense(1))

        self.model.compile(loss='mean_squared_error', optimizer='adam')
        self.model.summary()

    def find_maximal_div(bat_s, x):
        import numpy as np
        for i in np.arange(x):
            if bat_s * i <= x:
                max_div = bat_s * i
            else:
                break
        return max_div

    def pre_training(self, dataset):
        # training_threshold specifies the limit between training and testing
        # however, the threshold should be devided by the batch size (we will find the max value with find_max_div)
        max_training_index = Model.find_maximal_div(self.batch_size, len(dataset.y))
        self.model.fit(dataset.x[0:max_training_index],
                       dataset.y[0:max_training_index],
                       batch_size=self.batch_size,
                       epochs=2,
                       verbose=2,
                       shuffle=True)
        return dataset.x[0:max_training_index], dataset.y[0:max_training_index]

    def fit(self, dataset, training_index):
        # training_threshold specifies the limit between training and testing
        # however, the threshold should be devided by the batch size (we will find the max value with find_max_div)
        max_training_index = Model.find_maximal_div(self.batch_size, training_index)
        self.model.fit(dataset.x[0:max_training_index],
                       dataset.y[0:max_training_index],
                       batch_size=self.batch_size,
                       epochs=20,
                       verbose=2,
                       shuffle=True)
        return dataset.x[0:max_training_index], dataset.y[0:max_training_index]

    def predict(self, dataset, training_index):
        max_training_index = Model.find_maximal_div(self.batch_size, training_index)
        end_testing_index = Model.find_maximal_div(self.batch_size, len(dataset.y) - training_index)
        return dataset.x[max_training_index:(max_training_index+end_testing_index)], dataset.y[max_training_index:(max_training_index+end_testing_index)], self.model.predict(dataset.x[max_training_index:(max_training_index+end_testing_index)], batch_size=self.batch_size)

