import numpy as np

class RegressionDataset:

    def __init__(self, timesteps, data_dim):
        self.timesteps = timesteps
        self.data_dim = data_dim
        self.x = np.ndarray((0, timesteps, data_dim))
        self.y = np.ndarray((0, 1))

    def append(self, experiment, input_list=['power', 'rf', 'hr', 'cadence'], output='vo2'):

        for q in range(len(experiment)):
            data = experiment[q]

            for i in range(data.shape[0] - self.timesteps - 1):
                new_x_set = []
                for _, l in enumerate(input_list):
                    new_x_set.append(np.reshape(data[l].values[i:i + (self.timesteps)], (1, self.timesteps, 1)))

                new_x_set = np.concatenate(new_x_set, axis=2)
                new_y_set = np.reshape(data[output].values[i + self.timesteps], (1, 1))

                self.x = np.concatenate([self.x, new_x_set], axis=0)
                self.y = np.concatenate([self.y, new_y_set], axis=0)


